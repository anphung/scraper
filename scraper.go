// Package main is a sample of how to use scraper
package main

import (
	"strconv"

	"bitbucket.org/anphung/scraper/api"
	"bitbucket.org/anphung/scraper/handler"
	"bitbucket.org/anphung/scraper/utils"
)

func main() {
	// Create scraper server
	port := utils.Getenv("SCRAPER_PORT", "8080")
	server := api.NewScraper()

	// Get timeout for this Amazon handler
	// Default: 10 seconds.
	timeout := 0
	s := utils.Getenv("SCRAPER_AMAZON_TIMEOUT", "10")
	i, err := strconv.Atoi(i)
	if err == nil {
		timeout = i
	} else {
		panic(err)
	}

	// Get proxies for this Amazon handler
	s = utils.Getenv("SCRAPER_AMAZON_PROXIES", "")
	proxies := utils.SplitTrim(s, " ")
	// Last empty item is using no proxy
	proxies = append(proxies, "")

	server.Bind(
		"movie/amazon/:id",
		handler.Amazon(
			"http://www.amazon.de/gp/product/:id",
			timeout,
			proxies))

	// Start server
	server.Run(port)
}
