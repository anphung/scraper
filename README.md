#scraper

## Usage manual
```console
go run scrape.go
```
```console
SCRAPER_AMAZON_PROXIES="1.2.3.4:1234" SCRAPER_AMAZON_TIMEOUT=10 SCRAPER_PORT=8080 go run scraper.go
```
Then, try some of these addresses:
```
http://localhost:8080/movie/amazon/B00KY1U7GM
http://localhost:8080/movie/amazon/B00K19SD8Q
```