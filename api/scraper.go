// Package implements simple scraper for Amazon Prime.
package api

import (
	"fmt"

	log "github.com/Sirupsen/logrus"
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
)

// Scraper is a API server that can scrape any URL input
type Scraper struct {
	*echo.Echo // API server
}

// NewScraper initializes and returns new scraper server
func NewScraper() *Scraper {
	// Our server
	e := echo.New()
	e.SetDebug(true)

	s := &Scraper{
		e,
	}

	return s
}

// Bind binds route to hander
func (s *Scraper) Bind(r string, h func(echo.Context) error) {
	s.Echo.Get(r, h)
}

// Run runs scraper server
func (s *Scraper) Run(port string) {
	log.Printf("Listening on port %s", port)
	s.Echo.Run(standard.New(fmt.Sprintf(":%s", port)))
}
