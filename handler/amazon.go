// Package handler contains echo handler for each specific website.
package handler

import (
	"bytes"
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	log "github.com/Sirupsen/logrus"
	"github.com/labstack/echo"

	"bitbucket.org/anphung/scraper/utils"
)

type AmazonResponse struct {
	Title       string   `json:"title"`
	ReleaseYear string   `json:"release_year"`
	Actors      []string `json:"actors"`
	Poster      string   `json:"poster"`
	SimilarIDs  []string `json:"similar_ids"`
}

func isBotChecked(s string) bool {
	return strings.Contains(s, "Bot Check")
}

// get makes a get request to url and return the body as string
// of the response.
func getWithProxy(l string, proxies []string, timeout int) ([]byte, error) {
	if timeout < 1 {
		timeout = 1
	}

	// Iterate over each proxy until one success
	for _, proxy := range proxies {
		// Build http client
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		if proxy != "" {
			proxy = "http://" + proxy
			proxyURL, err := url.Parse(proxy)
			if err != nil {
				continue
			}
			tr.Proxy = http.ProxyURL(proxyURL)
		}
		netClient := &http.Client{
			Timeout:   time.Second * time.Duration(timeout),
			Transport: tr,
		}

		// Start requesting
		resp, err := netClient.Get(l)
		if err != nil {
			log.Error(err)
			continue
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Error(err)
			continue
		}
		s := string(body)

		if !isBotChecked(s) {
			return body, nil
		}
	}

	return nil, errors.New("out of proxy")
}

// Amazon wraps around a handler that fetches real Amazon URL and
// return processed data in JSON format.
func Amazon(l string, timeout int, proxies []string) func(c echo.Context) error {
	return func(c echo.Context) error {
		// Initialize response
		resp := &AmazonResponse{
			Actors:     []string{},
			SimilarIDs: []string{},
		}

		// Substitue params to Amazon request
		newL := l
		for _, n := range c.ParamNames() {
			newL = strings.Replace(newL, ":"+n, c.Param(n), -1)
		}

		// Fetch Amazon
		body, err := getWithProxy(newL, proxies, timeout)
		if err != nil {
			log.Error(err)
			return c.String(http.StatusInternalServerError, "Internal server error!")
		}
		r := bytes.NewReader(body)

		// Parse DOM
		doc, err := goquery.NewDocumentFromReader(r)
		if err != nil {
			log.Error(err)
			return c.String(http.StatusInternalServerError, "Internal server error!")
		}
		// Get release year and title
		s := doc.Find("#aiv-content-title .release-year").First()
		resp.ReleaseYear = strings.TrimSpace(s.Text())
		s = doc.Find("#aiv-content-title").First()
		resp.Title = strings.TrimSpace(s.Children().Remove().End().Text())
		// Get poster
		s = doc.Find(".dp-meta-icon-container img").First()
		resp.Poster, _ = s.Attr("src") // in case error Poster will be ""
		// Get actors
		s = doc.Find(".dv-info .dv-meta-info dd").First()
		resp.Actors = utils.SplitTrim(s.Text(), ",")
		// Get similar ids
		s = doc.Find(".dv-wdgt ul li").Each(func(i int, se *goquery.Selection) {
			id, _ := se.Attr("data-asin")
			if id != "" {
				resp.SimilarIDs = append(resp.SimilarIDs, id)
			}
		})

		return c.JSON(http.StatusOK, resp)
	}
}
