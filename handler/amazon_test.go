package handler

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/stretchr/testify/assert"
)

func TestIsBotChecked(t *testing.T) {
	cases := []struct {
		in   string
		want bool
	}{
		{"", false},
		{"abc", false},
		{"abdsadas Bot Checkkldslakdkac", true},
	}

	for _, c := range cases {
		got := isBotChecked(c.in)
		assert.Equal(t, c.want, got)
	}
}

func estGetWithProxy(t *testing.T) {
	outOfProxyErr := errors.New("out of proxy")

	cases := []struct {
		inBody   string
		proxies  []string
		timeout  int
		wantBody string
		wantErr  error
	}{
		{"hello", []string{""}, 5, "hello\n", nil},
		{"hello", []string{}, 5, "", outOfProxyErr},
		// Test bot checked
		{"Bot Checked", []string{""}, 5, "", outOfProxyErr},
		{"Bot Checked", []string{}, 5, "", outOfProxyErr},
		{"Bot Checked", []string{"1.1.1.1:2", ""}, 1, "", outOfProxyErr},
		// Test proxies
		{"hello", []string{"1.1.1.1:2", ""}, 0, "hello\n", nil},
		{"hello", []string{"1.1.1.1:2"}, 0, "", outOfProxyErr},
	}

	for _, c := range cases {
		// Build test server
		ts := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintln(w, c.inBody)
			}))

		gotBody, gotErr := getWithProxy(ts.URL, c.proxies, c.timeout)
		assert.Equal(t, c.wantBody, string(gotBody))
		assert.Equal(t, c.wantErr, gotErr)
		ts.Close()
	}
}

func TestAmazon(t *testing.T) {
	e := echo.New()

	outOfProxyErr := errors.New("out of proxy")
	_ = outOfProxyErr

	cases := []struct {
		apiReq     string
		inBody     string
		proxies    []string
		timeout    int
		wantBody   string
		wantErr    error
		wantStatus int
	}{
		{"movie/amazon/B00KY1U7GM",
			`
<h1 id="aiv-content-title"> Let me In [dt./OV] <span class="release-year" >2011</span> </span> </h1> <div class="dv-dp-packshot js-hide-on-play-left"> <div class="dp-img-bracket dv-bottom-shadow"> <div class="dp-meta-icon-container"> <div class="dv-sash dv-sash-prime"> Auf Prime erhältlich </div> <img alt="" src="https://images-eu.ssl-images-amazon.com/images/I/41uAkTEwckL._SX200_QL80_.jpg" width="200" onload="if (typeof(setCSMReq) === 'function') { setCSMReq('af'); setCSMReq('cf'); }if (typeof uex === 'function') uex('ld', 'image-block', {wb:1})" onerror="this.onerror=null;if (typeof(setCSMReq) === 'function') { setCSMReq('af'); setCSMReq('cf'); }if (typeof uex === 'function') uex('ld', 'image-block', {wb:1});if (typeof ue !== 'undefined' && ue.tag)ue.tag('image-error')"> </div> </div> </div> <div class="dv-info"> <div class="dv-simple-synopsis dv-extender" data-extender="{&quot;numOfLines&quot;:3,&quot;maxNumOfLines&quot;:4,&quot;strings&quot;:{&quot;seeLess&quot;:&quot;Weniger anzeigen&quot;,&quot;seeMore&quot;:&quot;Mehr anzeigen&quot;}}"> <p>Herausragendes englischsprachiges Remake des schwedischen Kulthits &quot;So finster die Nacht&quot;, in dem sich ein zwölfjähriger Einzelgänger mit einem gleichaltrigen Mädchen anfreundet, das tatsächlich ein Vampir ist. </p> </div> <dl class="dv-meta-info size-small"> <dt>Darsteller:</dt> <dd> Chloe Grace Moretz, Kodi Smit-McPhee, Richard Jenkins </dd> <dt>Laufzeit:</dt> <dd> 1 Stunde, 51 Minuten </dd> </dl> <p class="meta-text">Verfügbar auf <a class="dv-toggle" href="https://www.amazon.de/gp/video/ontv/devices/ref=atv_dp_hd_dev">unterstützten Geräten</a></p> </div> </div>
<div class="dv-wdgt dv-wdgt-carousel carousel cf" data-context-touch-available="true"> <ul class="shelf cf" data-shelf-type='' data-context-link-text='' data-context-link-url='' data-reftag-prefix='dv_web_'> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00EVATZAQ' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00EVATZAQ/ref=pd_cbs_318_1'> <img alt="So finster die Nacht [dt./OV]" height="200" width="142" src="https://images-na.ssl-images-amazon.com/images/I/410vRme+2xL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01HDWLK2E' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01HDWLK2E/ref=pd_cbs_318_2'> <img alt="MICHAELA" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/714PYhoNHNL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01EXKCVAO' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01EXKCVAO/ref=pd_cbs_318_3'> <img alt="Animus: Das verräterische Herz [dt./OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/91FNcbEldrL._UY200_RI_UY200_.jpg" /> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01EU0NXTA' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01EU0NXTA/ref=pd_cbs_318_4'> <img alt="Room 33 [OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/81ZLV4m7AvL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00EVAZKC8' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00EVAZKC8/ref=pd_cbs_318_5'> <img alt="Bedingungslos" height="200" width="142" src="https://images-na.ssl-images-amazon.com/images/I/41X3fWDTl8L._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00U0FFIUC' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00U0FFIUC/ref=pd_cbs_318_6'> <img alt="Exists" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/81gHb0h4i7L._UY200_RI_UY200_.jpg" /> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01GGJ16IS' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01GGJ16IS/ref=pd_cbs_318_7'> <img alt="Cut Her Out [OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/91vOqmPs03L._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01F0KWRGY' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01F0KWRGY/ref=pd_cbs_318_8'> <img alt="Deadly Intent [OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/81gA-WgYA0L._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01FY9G36Q' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01FY9G36Q/ref=pd_cbs_318_9'> <img alt="The Creepy Doll [OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/71oRJkX1NPL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01IQ2KHAQ' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01IQ2KHAQ/ref=pd_cbs_318_10'> <img alt="Lost Soul [OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/81B+i1TPt5L._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00FYV2LRU' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00FYV2LRU/ref=pd_cbs_318_11'> <img alt="Durst" height="200" width="142" src="https://images-na.ssl-images-amazon.com/images/I/41eTkuGjprL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00S20DS36' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00S20DS36/ref=pd_cbs_318_12'> <img alt="Erlöse Uns Von Dem Bösen" height="200" width="133" src="https://images-na.ssl-images-amazon.com/images/I/919SifX7AXL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01HJ3BURM' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01HJ3BURM/ref=pd_cbs_318_13'> <img alt="Blood + Roses [OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/81EjtNs3MLL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00HX04EZC' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00HX04EZC/ref=pd_cbs_318_14'> <img alt="Das Ding [dt./OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/81AO94LFQmL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00UAY3LV6' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00UAY3LV6/ref=pd_cbs_318_15'> <img alt="The Last House on the Left" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/91apWIKAVNL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01IAFSHK6' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01IAFSHK6/ref=pd_cbs_318_16'> <img alt="Endlose Stille" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/61YRSp2q3fL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00GSCPW4I' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00GSCPW4I/ref=pd_cbs_318_17'> <img alt="Der Fluch der 2 Schwestern [dt./OV]" height="200" width="142" src="https://images-na.ssl-images-amazon.com/images/I/51qzEbEDbjL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00W41N3L6' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00W41N3L6/ref=pd_cbs_318_18'> <img alt="Housebound [dt./OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/91cidwubKxL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B01J2PDOPQ' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B01J2PDOPQ/ref=pd_cbs_318_19'> <img alt="Living Dead Lock Up 2 (2007) Full Movie V2 [OV]" height="200" width="150" src="https://images-na.ssl-images-amazon.com/images/I/8147nnW7CWL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> <li class="packshot downloadable_movie dv-universal-hover-enabled  " data-asin='B00I9T0AUY' data-why-reasons='' data-type='downloadable_movie' data-ajax-request='20' data-render-hover='400'> <a href='https://www.amazon.de/gp/product/B00I9T0AUY/ref=pd_cbs_318_20'> <img alt="Verflucht [dt./OV]" height="200" width="143" src="https://images-na.ssl-images-amazon.com/images/I/81Jk0mNAfvL._UY200_RI_UY200_.jpg" /> <span class="packshot-sash packshot-sash-prime"></span> </a> </li> </ul> </div>
			`,
			[]string{""},
			5,
			"{\"title\":\"Let me In [dt./OV]\",\"release_year\":\"2011\",\"actors\":[\"Chloe Grace Moretz\",\"Kodi Smit-McPhee\",\"Richard Jenkins\"],\"poster\":\"https://images-eu.ssl-images-amazon.com/images/I/41uAkTEwckL._SX200_QL80_.jpg\",\"similar_ids\":[\"B00EVATZAQ\",\"B01HDWLK2E\",\"B01EXKCVAO\",\"B01EU0NXTA\",\"B00EVAZKC8\",\"B00U0FFIUC\",\"B01GGJ16IS\",\"B01F0KWRGY\",\"B01FY9G36Q\",\"B01IQ2KHAQ\",\"B00FYV2LRU\",\"B00S20DS36\",\"B01HJ3BURM\",\"B00HX04EZC\",\"B00UAY3LV6\",\"B01IAFSHK6\",\"B00GSCPW4I\",\"B00W41N3L6\",\"B01J2PDOPQ\",\"B00I9T0AUY\"]}",
			nil,
			http.StatusOK,
		},
		{"movie/amazon/B00KY1U7GM",
			"hello",
			[]string{""},
			5,
			"{\"title\":\"\",\"release_year\":\"\",\"actors\":[\"\"],\"poster\":\"\",\"similar_ids\":[]}",
			nil,
			http.StatusOK,
		},
		{"movie/amazon/B00KY1U7GM",
			"Bot Checked",
			[]string{""},
			5,
			"Internal server error!",
			nil,
			http.StatusInternalServerError,
		},
	}

	for _, c := range cases {
		// Build test server
		ts := httptest.NewServer(http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintln(w, c.inBody)
			}))

		req, err := http.NewRequest(echo.GET, c.apiReq, nil)
		assert.NoError(t, err)
		rec := httptest.NewRecorder()
		ct := e.NewContext(
			standard.NewRequest(req, e.Logger()),
			standard.NewResponse(rec, e.Logger()))
		err = Amazon(ts.URL, c.timeout, c.proxies)(ct)
		assert.Equal(t, c.wantStatus, rec.Code)
		assert.Equal(t, c.wantBody, rec.Body.String())
		assert.Equal(t, c.wantErr, err)
	}

}
