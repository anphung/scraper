package utils

import (
	"os"
	"strings"
)

// Getenv returns values of environment variable, if not found returns def
func Getenv(name, def string) string {
	v := os.Getenv(name)
	if v == "" {
		v = def
	}

	return v
}

// collect is produces a new array of values by mapping
// each value in list through a transformation function
// Source: http://monksealsoftware.com/golang-collectmap-function-for-arrays-and-slices/
func collect(list []string, f func(string) string) []string {
	result := make([]string, len(list))
	for i, item := range list {
		result[i] = f(item)
	}
	return result
}

// SplitTrim splits string s by de, and trim them by whitespace.
func SplitTrim(s, de string) []string {
	// Split by delimeter de
	l := strings.Split(s, de)
	// Trim each element by whitespace
	l = collect(l, strings.TrimSpace)
	return l
}
