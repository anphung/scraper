package utils

import (
	"os"
	"reflect"
	"runtime"
	"strings"
	"testing"
)

func TestGetenv(t *testing.T) {
	cases := []struct {
		name string
		def  string
		want string
	}{
		{"", "", ""},
		{"abc", "", ""},
		{"", "123", "123"},
		{"test", "456", "456"},
		{"test", "1", "456"},
	}

	os.Setenv("test", "456")
	for _, c := range cases {
		got := Getenv(c.name, c.def)
		if got != c.want {
			t.Errorf("Getenv(%q, %q) == %q, want %q",
				c.name, c.def, got, c.want)
		}
	}
}

func TestSplitTrim(t *testing.T) {
	cases := []struct {
		in   string
		de   string
		want []string
	}{
		{"1, 2, 3", ",", []string{"1", "2", "3"}},
		{"1, 2, 3", ", ", []string{"1", "2", "3"}},
		{"1,  2,  3", ",", []string{"1", "2", "3"}},
		{"   1,  2,  3  ", ",", []string{"1", "2", "3"}},
	}
	for _, c := range cases {
		got := SplitTrim(c.in, c.de)
		if !reflect.DeepEqual(got, c.want) {
			t.Errorf("SplitTrim(%q, %q) == %q, want %q",
				c.in, c.de, got, c.want)
		}
	}
}

func TestCollect(t *testing.T) {
	cases := []struct {
		in   []string
		f    func(string) string
		want []string
	}{
		{[]string{}, strings.ToUpper, []string{}},
		{[]string{""}, strings.ToUpper, []string{""}},
		{[]string{"1 ", " hello"}, strings.ToUpper, []string{"1 ", " HELLO"}},
		{[]string{" 1 ", " he y "}, strings.TrimSpace, []string{"1", "he y"}},
	}
	for _, c := range cases {
		got := collect(c.in, c.f)
		if !reflect.DeepEqual(got, c.want) {
			fName := runtime.FuncForPC(reflect.ValueOf(c.f).Pointer()).Name()
			t.Errorf("collect(%q, %q) == %q, want %q",
				c.in, fName, got, c.want)
		}
	}
}
